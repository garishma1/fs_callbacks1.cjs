const fs =require("fs")
const path = require("path")

function createRandomJsonFiles(){
    try{
        fs.mkdir("./randomFiles",(err)=>{
            if(err){
                console.log(err)
            }else{
                console.log("directory created successfully....")
            }
        })
        for(let index = 0;index<10;index++){
            let content=`This is file${index}`;
            let fileName=`file.txt${index}.json`;
            let dir="./randomFiles";
            let filePath=path.join(dir,fileName);
            fs.writeFile(filePath,JSON.stringify(content),(err)=>{
                if(err){
                    console.log(err);
                }else{
                    console.log("File created Successfully...");
        }
        fs.unlink(filePath,(err)=>{
            if(err){
                console.log(err);
            }else{
                console.log("File Deleted Successfully...");
            }
            
        })
    });

    
};
    }catch(error){
        console.log(error);
    }

};

 function removingDirectory(){
    try{
    fs.rm("./randomFiles",{recursive:true},(error)=>{
        if(error){
            console.log(error);
        }else{
            console.log("Directory Deleted Successfully ...");
        }
    });
}catch(error){
    console.log(error);
}
 };

 //createRandomJsonFiles()

 //removingDirectory()

 module.exports={
    createRandomJsonFiles,
    removingDirectory
 }

    


    

