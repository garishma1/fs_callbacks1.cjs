const fs=require("fs");

function readingAndDeletingFiles(){
    try{
    let fileNames=[];
    fs.readFile("../../Downloads/lipsum_1.txt","utf-8",(err,data)=>{
        if(err){
            console.log(err);
        }else{
            console.log("lipsum data readed successfully ...");

        }
        let upperCase = data.toUpperCase();
        let upperCaseFileName = "./upperCase.txt";
        let FilePath = "./filenames.txt";

        fs.writeFile(upperCaseFileName,upperCase,(err)=>{
            if(err){
                console.log(err);
            }else{
                console.log("uppercase data written successfully ...");
            }

            fs.appendFile(FilePath,upperCaseFileName+"\n",(err)=>{
                if(err){
                    console.log(err);
                }else{
                    console.log("uppercase filename written successfully ...");
                }

                fileNames.push(upperCaseFileName);

                fs.readFile(upperCaseFileName,"utf-8",(err,upperCaseData)=>{
                    if(err){
                        console.log(err);
                    }else{
                        console.log("uppercase data readed succesfully ...");
                    }

                    let lowerCase=upperCaseData.toLowerCase();
                    let sentence=JSON.stringify(lowerCase.split(" "));
                    let sentenceFileName="./sentence.txt";
                    fs.writeFile(sentenceFileName,sentence,(err)=>{
                        if(err){
                            console.log(err);
                        }else{
                            console.log("sentence file created successfully ...");
                        }

                        fs.appendFile(FilePath,sentenceFileName+"\n",(err)=>{
                            if(err){
                                console.log(err);
                            }else{
                                console.log("sentence filename written successfully ...")
                            }

                            fileNames.push(sentenceFileName);

                            fs.readFile(sentenceFileName,"utf-8",(err,sentenceData)=>{
                                if(err){
                                    console.log(err);
                                }else{
                                    console.log("sentencedata readed successfully ...");

                                };
                                
                                let arrayData=JSON.parse(sentenceData);
                                let sortingContent=arrayData.sort((a,b)=>{
                                    return a-b;
                                });
                                let sortingFileName="./sorting.txt";
                                fs.writeFile(sortingFileName,JSON.stringify(sortingContent),(error)=>{
                                    if(err){
                                        console.log(err);
                                    }else{
                                        console.log("sorting file created successfully ...");
                                    }

                                    fs.appendFile(FilePath,sortingFileName+"\n",(error)=>{
                                        if(err){
                                            console.log(err);
                                        }else{
                                            console.log("sorting filename written successfully ...")
                                        }

                                        fileNames.push(sortingFileName);

                                        fileNames.forEach((files)=>{
                                            fs.readFile(files,"utf-8",(err)=>{
                                                if(err){
                                                    console.log(err);
                                                }else{
                                                    console.log(` ${files} readed successfully ...`)
                                                }

                                                fs.unlink(files,(err)=>{
                                                    if(err){
                                                        console.log(err);
                                                    }else{
                                                        console.log(` ${files} deleted successfully ...`);
                                                    }
                                                })
                                            })
                                        });

                                        fs.unlink(FilePath,(err)=>{
                                            if(err){
                                                console.log(err);
                                            }else{
                                                console.log("filenames deleted successfully ...")
                                            }
                                        })
                                    })
                                })
                            })
                        })
                    })
                })
            })
        })
        
    });
}catch(err){
    console.log(err);
}

};



module.exports=readingAndDeletingFiles;


                     